<?php

/**
 * Tweets controller class. Controllers the flow of the tweets application.
 *
 * @author     James Sedillo <jin91706@yahoo.com>
 * @version    Release: 1.0.0
 */

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Abraham\TwitterOAuth\TwitterOAuth;

class Tweets extends Controller
{
    
    /**
     * @return View object with past tweet searches.
     *
     */
    public function index() {

        $tweets = Tweet::all();

    	return view('tweets', ['tweets' => $tweets]);
    }

    /**
     *  @param Request object that holds request parameters.
     *  
     *  @return string json string
     */
    public function search(Request $request) {

        $search = $request->input('search');
    	$connection = new TwitterOAuth(env('TWITTER_KEY'), env('TWITTER_SECRET'));
    	$statuses = $connection->get("search/tweets", ["q" => "#".$search.""]);

        $tweet = new Tweet;
        $tweet->content = $search;
        $tweet->save();
        $last_id = $tweet->id;

    	$collection = [];
        $statuses_arr = [];

        if (!empty($statuses->statuses)) {
            foreach ($statuses->statuses as $key => $value) {
            $status = [
                    'handle' => $value->user->screen_name, 
                    'create_at' => $value->created_at, 
                    'content' => $value->text
                ];
                $statuses_arr[] = $status;
            }
        }
        $collection['statuses'] = $statuses_arr;
        $collection['search'] = $search;
        $collection['last_id'] = $last_id;
    	return response()->json($collection);
    }

    /**
     *  @param Request object that holds id parameter input.
     *  
     */    
    public function delete(Request $request) {

        $id = $request->input('id');
        Tweet::destroy($id);
    }

    /**
     *  @param Request object that holds search parameter input.
     *
     */
    public function api_search(Request $request) {

        $search = $request->input('search') . '%';
        $tweets = DB::table('tweets')
                ->where('content', 'like', $search)
                ->get();

        $collection = [];

        if (!empty($tweets)) {
            foreach ($tweets as $tweet) {
                $collection[] = [
                    'content' => $tweet->content,
                    'id' => $tweet->id,
                ];
                
            }
        }
        
        return response()->json($collection);
    }

}
