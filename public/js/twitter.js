$(function() {

	$(document).on('shown.bs.tab', function(){
		if ($(document).height() > $(window).height()) {
	    	$('header').css('margin-bottom', '67px');
		} else {
			$('header').css('margin-bottom', '50px');
		}
	});

	$(document).on('click', '.remove_tweet', function(){

		var id_val = $(this).attr('data-id');
		$.ajax({
			method: "POST",
			url: "tweets/delete",
			data: { id: id_val },
			dataType: "json",
			headers: {
        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
		})
		.done(function( data ) {
			
		});

		$(this).closest('div').remove();
	});

	$(document).on('click', '.add_tweet', function (e) {
  		
  		location.reload();
	});


	$( "#search_form" ).submit(function( event ) {
  		event.preventDefault();
  		$('#spinner_con').css('display', 'block');
  		$.ajax({
			method: "POST",
			url: "tweets/search",
			data: { search: $('#search').val() },
			dataType: "json",
			headers: {
        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
		})
		.done(function( data ) {

			if (data == null || data.length === 0) {

				$('#spinner_con').css('display', 'none');
				var html = 'No results';
				$( "#twitter_results" ).html( html );
				$('#clear').css('display', 'block');

			} else {
			
				$('#spinner_con').css('display', 'none');
				var html = '';
				$.each(data.statuses, function(index, element) {
					
					html += "<div>Twitter Handle: " + element.handle +"</div>" +
				    	  "<div>Created at: " + element.create_at + "</div>" +
				    	  "<div>Content: " + element.content + "</div>" +
				    	  "<br /><br />";
	   				
				});

				$( "#twitter_results" ).html( html );
				$( "#tweet_searches" ).append( '<div><a href="https://twitter.com/search?q=%23' 
													+ data.search + '">#' + data.search + '</a>' +
						'<span style="margin-left: 25px;">'+
						'<span class="add_tweet" data-id="' + data.last_id + '">+</span>'+' / '+
						'<span class="remove_tweet" data-id="' + data.last_id + '">-</span></span></div>' );
				$('#clear').css('display', 'block');

				if ($(document).height() > $(window).height()) {
			    	$('header').css('margin-bottom', '67px');
				} else {
					$('header').css('margin-bottom', '50px');
				}
			}
			
		});
	});


	$( "#filter" ).keyup(function( event ) {
		event.preventDefault();

		$.ajax({
			method: "POST",
			url: "tweets/api_search",
			data: { search: $('#filter').val() },
			dataType: "json",
			headers: {
        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
		})
		.done(function( data ) {

			if (data == null || data.length === 0) {

				var html = 'No results';
				$( "#tweet_searches" ).html( html );

			} else {
				var html = '';
				$.each(data, function(index, element) {

					html += '<div>' +
    					'<a href="https://twitter.com/search?q=%23' + element.content + '">#' + element.content + '</a>' +
    					'<span class="ar-tweet-con"><span class="add_tweet" data-id="' + element.id + '">+</span>' + 
    					' / <span class="remove_tweet" data-id="' + element.id + '">-</span></span>' +
    				'</div>';
				});
				
				$( "#tweet_searches" ).html( html );
			}
			
		});
	});
	
});