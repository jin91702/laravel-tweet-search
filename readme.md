
# Tweet Search

## Install

Create a .env file in the root directory. 

Generate app key and add Db connection settings.

Create a database called 'tweets' and a table called 'tweets'
````sql
CREATE TABLE `tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
SELECT * FROM tweets.tweets;

````
