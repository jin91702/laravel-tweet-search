<!DOCTYPE html>
<html>

    <head>
        <title>Tweets - @yield('title')</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/css/jquery-ui.min.css"/>
        <link rel="stylesheet" href="/css/tweet.css"/>

        <style type="text/css">
            html, body {
                height: 100%;
                font-family: Optima,Segoe,"Segoe UI",Candara,Calibri,Arial,sans-serif
            }
            body {
                display: flex;
                flex-direction: column;
            }
            .content {
                flex: 1 0 auto;
            }
            footer {
                flex-shrink: 0;
                min-height: 100px; 
                background-color: #809fad;
            }
            header {
                margin-bottom: 50px;
                min-height: 100px; 
                background-color: #809fad;
            }
            small {
                color:red;
                margin-left: 5px;
            }
            .main-title {
                margin-top: 15px;
                color: #eee;
            }
            .index-container a, .index-container a:hover {
                color: #212529;
                text-decoration: none;
            }
            .navbar {
                z-index: 5;
            }
            #tweet_searches span {
                cursor: pointer;
            }
        </style>
    </head>

    <body>
        <header>
            <div class="container">
                <div class="main-title">
                    <h2>Tweets</h2>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a class="nav-link" href="/">Home</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        
        <div class="container content">
            @yield('content')
        </div>
        
        <footer>
            
        </footer>

        <script src="/js/jquery-3.3.1.min.js"></script>
        <script src="/js/jquery-ui.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/twitter.js"></script>
    </body>
</html>