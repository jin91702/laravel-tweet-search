@extends('layouts.app')

@section('title', 'Hashtag Search')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

	<nav>
		<div class="nav nav-tabs" id="nav-tab" role="tablist">
	    	<a class="nav-item nav-link active" id="nav-search-tab" data-toggle="tab" href="#nav-search" role="tab" aria-controls="nav-search" aria-selected="true">Search Twitter</a>
	    	<a class="nav-item nav-link" id="nav-entries-tab" data-toggle="tab" href="#nav-entries" role="tab" aria-controls="nav-entries" aria-selected="false">Saved Entries</a>
	  </div>
	</nav>
	<div class="tab-content" id="nav-tabContent">
	  	<div class="tab-pane fade show active" id="nav-search" role="tabpanel" aria-labelledby="nav-search-tab">
	  		<br />
	  		<form id="search_form">
	  			#<input type="text" name="search" id="search"><span class="search_label">Hashtag Search</span>
	  			
	  		</form>
	  		
	  		<div id="spinner_con">
	  			<img src="/img/spinner-gif-13.gif">
	  		</div>
	  		<br />
	  		<a href="/" id="clear">Clear</a>
	  		<br />
			<div id="twitter_results"></div>

	  	</div>
	  	<div class="tab-pane fade" id="nav-entries" role="tabpanel" aria-labelledby="nav-entries-tab">
	  		<br />
	  		<form id="filter_form">
	  			#<input type="text" name="filter" id="filter"><span class="search_label">Filter Search</span>
	  		</form>
	  		<div id="tweet_searches">
	  			@foreach ($tweets as $tweet)
    				<div>
    					<a href="https://twitter.com/search?q=%23{{ $tweet->content }}" >#{{ $tweet->content }}</a>
    					<span class="ar-tweet-con"><span class="add_tweet" data-id="{{ $tweet->id}}">+</span> / <span class="remove_tweet" data-id="{{ $tweet->id }}">-</span></span>
    				</div>
				@endforeach
	  		</div>
	  		<br />
	  	</div>
	</div>

@endsection